/*
Navicat MySQL Data Transfer

Source Server         : DBlink
Source Server Version : 80011
Source Host           : localhost:3306
Source Database       : vue-chi

Target Server Type    : MYSQL
Target Server Version : 80011
File Encoding         : 65001

Date: 2018-07-25 11:45:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `article_table`
-- ----------------------------
DROP TABLE IF EXISTS `article_table`;
CREATE TABLE `article_table` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `author` varchar(16) COLLATE utf8mb4_general_ci NOT NULL,
  `author_src` varchar(64) COLLATE utf8mb4_general_ci NOT NULL,
  `title` varchar(32) COLLATE utf8mb4_general_ci NOT NULL,
  `post_time` int(11) NOT NULL,
  `content` text COLLATE utf8mb4_general_ci NOT NULL,
  `description` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `like_num` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of article_table
-- ----------------------------
INSERT INTO `article_table` VALUES ('1', 'Fiona', 'img/user.jpg', '苹果以用户隐私拒绝政府应用上架，iPhone 在印度面临退网', '1532337058', '苹果在印度的麻烦不断。\r\n\r\n为了应对骚扰电话和短信，印度电信管理局（TRAI）曾经推出名为 DND（Do Not Disturb）的应用，用户可以通过应用上报骚扰信息。\r\n\r\n然而苹果始终没有将这款应用通过审核，理由是应用读取并上传了用户的电话和短信，这是对用户隐私的侵犯。TRAI 则认为所谓隐私的使用应当由 iPhone 用户而不是苹果决定。\r\n\r\n矛盾从去年 9 月延续至今，据 India Today 报道，上周 TRAI 发布通告决定采取强制措施。 TRAI 要求所有电信运营商保证印度境内的所有智能手机都安装上 DND 的最新应用，即日起的 6 个月之内未有安装，电信运营商需要将设备从移动网络中移除。\r\n\r\nGoogle Play 早在 2016 年就通过 DND 应用，这等于给苹果下了最后时限，如果未能在 2019 年 1 月前通过 DND 的审核上架应用，所有在印度的 iPhone 都会被退网，3G、4G 和语音通讯都无法使用。\r\n\r\n目前苹果尚未就此作出回应。事实上去年苹果答应和 TRAI 共同研发一款防骚扰应用，随后没了下文。\r\n\r\niOS 12 增加了处理垃圾短信和通话的新功能，可能是个折衷方案。开发者可以利用苹果的接口创建应用，允许用户对骚扰电话、短信屏蔽和举报。不过这和 TRAI 的应用也有本质区别，是苹果而不是应用开发者掌握详细和具体的用户数据。\r\n\r\n苹果在印度的处境没有好转，根据 Counterpoint 的报告，今年上半年苹果在印度销售 100 万台手机，不到去年全年 320 万台的一半。目前苹果的市场占有率只有 2%。\r\n\r\n尽管销售主力是 iPhone SE 这样的老款机型，但苹果产品依然价格高昂。根据 IDC 的报告，印度市场排名前 5 的厂商分别是小米、三星、Vivo、OPPO 和荣耀，中国手机厂商销售价格低廉的智能手机，占据超过一半的市场份额。\r\n\r\n彭博社上周报道说，多位印度销售和分销部门主管已经陆续离开苹果，包括其商业渠道和终端市场业务负责人及电信运营商销售主管，苹果正打算重组印度销售团队。\r\n\r\n而苹果不得不面对价格更高的窘境。印度政府坚持手机厂商的生产本地化，目前苹果仅有 iPhone SE 在本地组装，零部件也全部为进口。去年底印度政府提高进口关税，包括 Apple Watch 在内的产品两个月内提高了两次价格。', '为了应对骚扰电话和短信，印度电信管理局（TRAI）曾经推出名为', '16');
INSERT INTO `article_table` VALUES ('2', 'sophia', 'img/user.jpg', '百余车厂机密数据泄露，特斯拉丰田福特大众都在内', '1532337273', '网络安全公司 UpGuard 发布报告称，100 多家车厂的机密数据遭到了泄露，其中包括通用汽车、菲亚特克莱斯勒、福特、特斯拉、丰田、蒂森克虏伯及大众等。\r\n\r\n数据泄漏的源头是一家名为 Level One Robotics 的公司的公共服务器。这家加拿大公司成立于 2000 年，是以上百余家车厂共同的服务器提供商。\r\n\r\n本次事故的始作俑者不是黑客，而是该公司自己。Level One 在一个可公开访问的服务器中使用了一种用于备份大型数据集的通用文件传输协议 rsync ，但却没有设定任何安全密码保护措施。通过该传输协议，任何用户都可无障碍访问其中的隐私数据，而且，连接到 rsync 端口的任何客户端都有权下载数据。\r\n\r\n本次遭到泄露的数据包括车厂发展规划蓝图、装配线原理图、工厂平面图和布局、机器人配置信息、ID 徽章请求表，VPN 访问请求表、客户合同材料、保密协议文件、员工驾驶证和护照的扫描件等，共计 157 千兆字节，包含近 47000 个文件。\r\n\r\nUpGuard 的安全团队称在 7 月 1 日发现了该漏洞，然后在 7 月 9 日联系到了 Level One。接到通知后的第二天，Level One 采取断网脱机的方式，暂时终止了数据泄露。\r\n\r\n目前不清楚是否已有人拿到了这些数据，Level One CEO 米兰·加斯科（Milan Gasko）拒绝讨论案件细节。\r\n\r\n“Level One 非常重视针对我们涉嫌暴露数据的指控，并正在努力对这一事件的程度和后果进行全面调查。”加斯科说，“为了保持调查的公正性，我们目前不会发表任何评论。”\r\n\r\n发现了这场大乌龙的 UpGuard 可以被看作是一个“白帽子”团队，其主管 Chris Vickery 称自己的职业就是“寻找无人看管的数据库”。此前被他发现含有漏洞的系统包括社会医疗记录、机场安检档案记录、酒店预订记录、恐怖分子筛选数据库和 8700 万墨西哥选民登记记录库等。\r\n\r\n在提醒数据库主管漏洞的存在之后，Chris Vickery 会在确保数据源得到保护的情况下公开整个事件。虽然没有企业或政府部门会希望 Chris Vickery 如此公开自己的错误，但是他坚持这么做。\r\n\r\n“如果我们一直沉默，网络安全不可能变好，”Vickery 说，“因为人们会试图掩盖错误，这伤害了我们的社会。除非让这些人意识到问题的存在，不然他们不会有任何改进。”\r\n\r\n但整场事件对于特斯拉、丰田、大众等车厂来说，就几乎是无端躺枪、飞来横祸了。《纽约时报》评论称，这件事给了大企业一个教训，让他们意识到第三方供应商很有可能成为泄露其核心机密的软肋。\r\n\r\n安全研究公司 Ponemon Institute 去年调查的企业中有 56％ 表示他们曾遇到过与供应商有关的数据泄露问题。随着越来越多的第三方公司轻松获得大企业数据库访问权，这种风险只会增加：该调查发现，平均每家公司有 470 家外部公司可以访问其敏感信息库，而一年前这一数字大约为 380。\r\n\r\n上个月，票务公司 Ticketmaster 称，数千名客户的付款信息被盗，原因是第三方公司 Inbenta 在 TicketMaster 网站上运行的聊天机器人软件存在漏洞。2013 年，黑客在泛欧实时全额自动清算系统 Target 的支付终端窃取了 4000 万客户的信用卡和借记卡信息，就是从 Target 的一家供暖和通风承包商处获得了该系统的入口。', '未遭黑客攻击，第三方系统开门揖盗。', '0');

-- ----------------------------
-- Table structure for `banner_table`
-- ----------------------------
DROP TABLE IF EXISTS `banner_table`;
CREATE TABLE `banner_table` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(32) COLLATE utf8mb4_general_ci NOT NULL,
  `sub_title` varchar(16) COLLATE utf8mb4_general_ci NOT NULL,
  `src` varchar(64) COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of banner_table
-- ----------------------------
INSERT INTO `banner_table` VALUES ('1', 'DVA 爱你哦！！！DVA', 'boom~ shakalaka', 'img/1.png');
INSERT INTO `banner_table` VALUES ('2', '法老之鹰大姐姐', 'rapha！！！', 'img/2.jpg');
INSERT INTO `banner_table` VALUES ('3', '翱翔于天际的法老之鹰', '天降正义！', 'img/3.jpg');
INSERT INTO `banner_table` VALUES ('4', '喂喂喂！法鸡 了解一下', '喂？挂啦？', 'img/4.jpg');

-- ----------------------------
-- Table structure for `user_table`
-- ----------------------------
DROP TABLE IF EXISTS `user_table`;
CREATE TABLE `user_table` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(32) COLLATE utf8mb4_general_ci NOT NULL,
  `pass_word` varchar(32) COLLATE utf8mb4_general_ci NOT NULL,
  `src` varchar(64) COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of user_table
-- ----------------------------
