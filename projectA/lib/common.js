function toDouble(n) {
    return n < 0 ? '0' + n : '' + n;
}

module.exports = {
    time2date: function (timeStamp) {
        let myDate = new Date();
        myDate.setTime(timeStamp * 1000);
        return myDate.getFullYear() + '-' + toDouble(myDate.getMonth() + 1) + '-' + toDouble(myDate.getDate()) + ' ' + toDouble(myDate.getHours()) + ':' + toDouble(myDate.getMinutes()) + ':' + toDouble(myDate.getSeconds())
    }
}